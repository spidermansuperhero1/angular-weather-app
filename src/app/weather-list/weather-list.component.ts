import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { Location } from '@angular/common';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import {Router} from '@angular/router';
import { Injectable } from '@angular/core';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})

export class WeatherListComponent implements OnInit {
  public weather = [];
  public errorMsg;
  public data:any=[]
  constructor(private _weatherService:WeatherService, private router: Router) {
     
   }

  ngOnInit() {
    this._weatherService.getWeather()
    .subscribe(data => {
      this.weather = data.list
     // localStorage.setItem('user', JSON.stringify(this.weather))  
    });

  }
  
   onSelect(weather){
    // this.router.navigate(['/weatherDetails',weather.dt_txt]);
    localStorage.setItem('key', JSON.stringify(weather)) 

   }

   
}
