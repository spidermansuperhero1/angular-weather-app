import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WeatherListComponent} from './weather-list/weather-list.component';
import {DetailsComponent} from './details/details.component';


const routes: Routes = [
  { path: '',component:WeatherListComponent,pathMatch:'full'},
  { path: 'weatherDetails', component:DetailsComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [WeatherListComponent]
