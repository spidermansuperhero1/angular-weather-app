import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable,of } from 'rxjs';
//import 'rxjs/add/operator/catch';
import { catchError, map, tap } from 'rxjs/operators';
//import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private _url : string = "https://samples.openweathermap.org/data/2.5/forecast/hourly?id=524901&appid=b6907d289e10d714a6e88b30761fae22"
  constructor(private http : HttpClient) { }

  getWeather() : Observable<any>{
    return this.http.get<any>(this._url)
    .pipe(
      catchError(this.handleError<any>('getWeather', []))
    );
   }

   private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
     // this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
